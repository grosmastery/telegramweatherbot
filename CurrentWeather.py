import requests
import json
import telebot
from typing import List
import os
from dotenv import load_dotenv

load_dotenv()


weather_api_key = os.environ.get('WEATHER_API_KEY')
bot_token = os.environ.get('BOT_TOKEN')
bot = telebot.TeleBot(bot_token)


class WeatherApi:
    def get_weather_data(self, location: str) -> json:
        url = f"http://api.openweathermap.org/data/2.5/weather?q={location}&appid={weather_api_key}"
        response = requests.get(url)

        if response.status_code == 200:
            return response.json()
        else:
            print(f"STATUS CODE: {response.status_code}")
            return 'error'

    def set_weather_data(self, weather_data: dict) -> List[str]:
        data_list = []
        if weather_data == 'error':
            return ["There are problems on the site, come back later."]
        elif weather_data:
            data_list.append(f"Description: {weather_data['weather'][0]['description']}")
            data_list.append(f"Feels like: {round(weather_data['main']['feels_like'] - 273.15, 1)} °C")
            data_list.append(f"Temperature: {round(weather_data['main']['temp'] - 273.15, 1)} °C")
            data_list.append(f"Humidity: {weather_data['main']['humidity']}%")
            data_list.append(f"Wind speed: {weather_data['wind']['speed']} m/s")
            return data_list
        else:
            return ["Could not retrieve weather data for that location."]


@bot.message_handler(regexp=r'^What is the weather in (.+)$')
def weather_telegram_bot(message):
    weather = WeatherApi()
    location = message.text.split()[-1]
    weather_data = weather.get_weather_data(location)
    set_weather_data = weather.set_weather_data(weather_data)
    bot.send_message(message.chat.id, "\n".join(set_weather_data))


if __name__ == "__main__":
    bot.polling()
