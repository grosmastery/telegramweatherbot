# **Weather Telegram Bot** 
This is a Python script that implements a Telegram bot to provide the current weather information for a given location using the OpenWeatherMap API.
## **Usage**
To use the bot, follow these steps:

1. Start a chat with your bot.
1. Send a message in the following format: `What is the weather in <city name>`.

## **Code Overview**
### **Dependencies**
- requests - Used to make HTTP requests to the OpenWeatherMap API.
- python-telegram-bot - A Python wrapper for the Telegram Bot API.
- python-dotenv - Used to load environment variables from a .env file.
## **Classes**

**`WeatherApi`**

This class provides two methods:

**`get_weather_data(location: str) -> json`**: This method makes a request to the OpenWeatherMap API and returns the JSON response. The location parameter is a string that represents the name of the city for which you want to get the weather information.

**`set_weather_data(weather_data: dict) -> List[str]`**: This method takes the JSON response from the get_weather_data method and returns a list of strings that represent the weather information for the given location.

### **Telegram Bot**
The bot uses the `python-telegram-bot` library to interact with the Telegram Bot API. It listens for messages that match the regular expression `'^What is the weather in (.+)$'` and responds with the weather information for the specified location.
